<?php
	session_start();	

	$dbconnector = 'mysql:host=localhost;dbname=audiosync';
	$dbuser = "root";
	$dbpwd = "12345678";
	$tableVideo = "VIDEO";
	$controlPwd = "mypwd";
	
	
	if(isset($_GET['pwd']) && $_GET['pwd'] == $controlPwd){
		$_SESSION['control'] = true;			
	}
	
	if(isset($_SESSION['control']) and $_SESSION['control'] == true and isset($_GET['c'])){
		
		$time = 0;
		
		$db = new PDO($dbconnector, $dbuser, $dbpwd);
		$stmt = $db->prepare("select Starttime from $tableVideo where Id = 1");
		$stmt->execute(array());
		if($r = $stmt->fetch()){
			$time = microtime(true) - htmlspecialchars($r[0], ENT_QUOTES);			
		}
		
		print "<script type=text/javascript >
			
				function httpGetAsync(theUrl, callback){
					var xmlHttp = new XMLHttpRequest();
					xmlHttp.onreadystatechange = function() { 
						if (xmlHttp.readyState == 4 && xmlHttp.status == 200)
							callback(xmlHttp.responseText);
					}
					xmlHttp.open('GET', theUrl, true); // true for asynchronous 
					xmlHttp.send(null);
				}
				
				function sync(video) {
					return httpGetAsync('server.php?time=' + (Date.now()/1000), function (response) 
					{ 
						var times = response.split(';');
						var rtt = (Date.now() / 1000) - times[2];		
						var currtime = times[1] - times[0] - rtt;
						//document.getElementById('out').innerText = rtt;
						if(Math.abs(video.currentTime - currtime) > 0.2){
							video.currentTime = times[1] - times[0] - (rtt/2); 	
						}		
						if(video.paused){
							video.play();
						}
					});
				}
				
			</script>
			<h1 id='out' ></h1>
			<video id='video' width='320' height='240' controls>
				  <source src='video.mp4' type='video/mp4'>
					Your browser does not support the video tag.
				</video>
				<script type=text/javascript>
					var video = document.getElementById('video');
				
					video.onplay = function() {
						httpGetAsync('server.php?c&play=' + video.currentTime, function(response) { })
					}
					video.onended = function() {
						video.play();
					}
				
					sync(video);
					window.setInterval(function(){
					  sync(video);					  
					}, 1000);
					
				</script>
		";	
	
		if(isset($_GET['play'])){		
			$db = new PDO($dbconnector, $dbuser, $dbpwd);
			$videotime = $_GET['play'];
			echo microtime(true) . "<br/>";
			$time = microtime(true) - $videotime;
			$title = "my video";
			echo $time;
			echo "<br/>";
			echo $videotime;
			$stmt = $db->prepare("insert into $tableVideo (Id, Title, Starttime) values (1, :title, :starttime)");
			if(!$stmt->execute(array(':title' => $title, ':starttime' => $time))){
				$stmt = $db->prepare("update $tableVideo set starttime = :starttime where id = 1");
				if(!$stmt->execute(array(':starttime' => number_format($time, 8, '.', '')))){	
					print "error saving start time";
				}
			}
		}
	}
	
	
	if(isset($_GET['time'])){
		$db = new PDO($dbconnector, $dbuser, $dbpwd);
							
		$clienttime = $_GET['time'];
		
		$stmt = $db->prepare("select Starttime from $tableVideo where Id = 1");
		$stmt->execute(array());
		if($r = $stmt->fetch()){
			$t = htmlspecialchars($r[0], ENT_QUOTES);
			print $t . ";" . microtime(true) . ";" . $clienttime;
		}
	}
?>