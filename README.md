This project is an attempt to synchronize a video and multiple sound clients.

Prerequisites:
	+ mySql database:
create table VIDEO (
	id INT(11) primary key,
	title VARCHAR(255),
	starttime DECIMAL(20,8)
) ENGINE = MEMORY;
		
	+ you need a functional php server
	+ set the db user credentials and other connection details in the server.php file
	+ open {yourdomain}/AudioSync/server.php?c&pwd={controlPwd} to start the video
	+ all clients can listen to the sound of the video by navigating to {yourdomain}/AudioSync/   (HTML5 is needed)